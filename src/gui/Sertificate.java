package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.TextField;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;

import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JComboBox;

import java.awt.Component;

import javax.swing.border.LineBorder;

import java.awt.Color;
import java.security.Certificate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle.Control;
import java.util.zip.Checksum;

import javax.swing.border.TitledBorder;
import javax.swing.JSplitPane;
import javax.swing.JSpinner;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.AbstractListModel;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JButton;

import cryptography.Controller;
import cryptography.X509CertData;


public class Sertificate extends JFrame {

	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private final JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
	private JPanel import_exportTab;
	private JPanel eksport_sertifikata;
	private JPanel userData;
	private JTextField cnText;
	private JTextField ouText;
	private JTextField orgText;
	private JTextField locText;
	private JTextField stateText;
	private JTextField countryText;
	private JTextField streetText;

	private X509CertData currentX509 = new X509CertData();
	private JCheckBox aesBox;
	private JCheckBox aesOutBox;
	private JTextField aliasiText;
	public LinkedList<String> aliasi;
	private JPanel alternativeData;
	private JTextField altIpText;
	private JTextField altdnsText;
	private JTextField altemailText;
	private JTextArea textAreaPregled;
	
	private TextField aesImportPass;
	private TextField aesExportPass;
	
	private JComboBox comboBox;
	private JComboBox comboBox_1;
	private JComboBox exportCombo;
	private JComboBox comboBox_pregled;
	
	
	public JMenuItem[] menuitems;
	private Controller controller = Controller.getInstance();
	private JMenu mnPregledajSertifikat;
	private JMenu mnSertifikati;
	private JMenuItem mntmDodajNovi;
	private JSpinner spinner;
	private JSpinner spinner_1;
	private JSpinner spinner_2;
	private JSpinner spinner_3;
	private JSpinner spinner_4;
	private JSpinner spinner_5;
	private JCheckBox basic;
	private JCheckBox alternative;
	private JCheckBox key;
	private JRadioButton basicYes;
	private JRadioButton basicNe;
	private JRadioButton alternativeYes;
	private JRadioButton alternativeNo;
	private JRadioButton keyYes;
	private JRadioButton keyNo;
	private JPanel ekstenz;
	private JCheckBox signature;
	private JCheckBox keyenc;
	private JCheckBox dataenc;
	private JCheckBox keycert;
	private JCheckBox nonrepud;
	private JCheckBox crl;
	private JCheckBox encOnly;
	private JCheckBox decOnly;
	private JCheckBox agree;
	private JPanel dodatnoPanel;
	private JCheckBox checkCA;
	private JTextField pathField;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Sertificate frame = new Sertificate();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public Sertificate() {
		super("Sertifikat");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 400);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(128, 128, 128)));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JMenuBar menuBar = new JMenuBar();
		contentPane.add(menuBar, BorderLayout.NORTH);
		
		 mnSertifikati = new JMenu("Sertifikati");
		menuBar.add(mnSertifikati);
		
		mntmDodajNovi = new JMenuItem("Dodaj novi");
		mnSertifikati.add(mntmDodajNovi);
		
		mntmDodajNovi.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				currentX509 = new X509CertData();
				restoreFields();
				
			}
		});
		
		 mnPregledajSertifikat = new JMenu("Pregledaj sertifikat");
		mnSertifikati.add(mnPregledajSertifikat);
		

		List<X509CertData> certs = controller.allCerts;
		
		int cnt = certs.size();
		int i = 0;
		menuitems = new JMenuItem[cnt];
		
		for(X509CertData x: certs)
		{
			String text = x.alias;
			menuitems[i] = new JMenuItem(text);
			mnPregledajSertifikat.add(menuitems[i]);
			menuitems[i].addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					JMenuItem i = (JMenuItem)e.getSource();
					currentX509 = Controller.getInstance().findCert(i.getLabel());
					restoreFields();
					
				}
			});
			i++;
		}
		
	
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 550, 350);
		contentPane.add(tabbedPane);
		
		JPanel tabPanel = new JPanel();
		tabbedPane.add("Generisanje para kljuceva", tabPanel);
		tabbedPane.setEnabledAt(0, true);
		tabPanel.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 550, 162);
		tabPanel.add(panel);
		panel.setForeground(Color.WHITE);
		panel.setBorder(new LineBorder(Color.GRAY));
		panel.setLayout(null);
	/*	JLabel version = new JLabel("v3");
		version.setName("v3");
		comboBox.addItem(version.getName());*/
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2);
		panel_2.setLayout(null);
		panel_2.setBounds(0, 0, 550, 55);
		
		JLabel lblDuzinaKljua = new JLabel("Duzina kljuceva");
		lblDuzinaKljua.setBounds(26, 28, 97, 14);
		panel_2.add(lblDuzinaKljua);
		lblDuzinaKljua.setHorizontalAlignment(SwingConstants.LEFT);
		
		textField = new JTextField();
		textField.setBounds(121, 25, 97, 20);
		panel_2.add(textField);
		
		
		JLabel lblVerzijaSertifikata = new JLabel("Verzija sertifikata");
		lblVerzijaSertifikata.setBounds(283, 28, 104, 14);
		panel_2.add(lblVerzijaSertifikata);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(397, 25, 55, 20);
		panel_2.add(comboBox);
		comboBox.addItem("v3");
		
		JPanel panel_3 = new JPanel();
		panel.add(panel_3);
		panel_3.setBounds(0,55,550, 100);

		panel_3.setLayout(null);
		
		spinner = new JSpinner();
		spinner.setBounds(87, 5, 37, 20);
		panel_3.add(spinner);
		
		JLabel lblNewLabel = new JLabel("do");
		lblNewLabel.setBounds(230, 5, 37, 14);
		panel_3.add(lblNewLabel);
		
		JLabel lblVaziOd = new JLabel("Vazi od");
		lblVaziOd.setBounds(27, 5, 64, 14);
		panel_3.add(lblVaziOd);
		
		spinner_1 = new JSpinner();
		spinner_1.setBounds(122, 5, 37, 20);
		panel_3.add(spinner_1);
		
		spinner_2 = new JSpinner();
		spinner_2.setBounds(161, 5, 56, 20);
		panel_3.add(spinner_2);
		
		spinner_3 = new JSpinner();
		spinner_3.setBounds(256, 5, 37, 20);
		panel_3.add(spinner_3);
		
		spinner_4 = new JSpinner();
		spinner_4.setBounds(293, 5, 37, 20);
		panel_3.add(spinner_4);
		
		spinner_5 = new JSpinner();
		spinner_5.setBounds(329, 5, 56, 20);
		panel_3.add(spinner_5);
		
		JLabel lblSerijskiBroj = new JLabel("Serijski broj");
		lblSerijskiBroj.setBounds(27, 40, 91, 14);
		panel_3.add(lblSerijskiBroj);
		
		textField_1 = new JTextField();
		textField_1.setBounds(109, 40, 108, 20);
		panel_3.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblKorisnik = new JLabel("Podaci o korisniku: ");
		lblKorisnik.setBounds(250, 40, 120, 15);
		panel_3.add(lblKorisnik);
		
		JButton korisnickiPodaci = new JButton("Unesite");
		korisnickiPodaci.setBounds(250, 60, 90, 20);
		panel_3.add(korisnickiPodaci);
		
		
		JLabel lblLternativno = new JLabel("Alternativni podaci: ");
		lblLternativno.setBounds(380, 40, 120, 15);
		panel_3.add(lblLternativno);
		
		final JButton alternativnoPodaci = new JButton("Izaberite");
		alternativnoPodaci.setBounds(380, 60, 90, 20);
		panel_3.add(alternativnoPodaci );
		
		initAlternativeData();
		
		alternativnoPodaci.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				altIpText.setText(currentX509.issuerAltNameIp);
				altdnsText.setText(currentX509.issuerAltNameDns);
				altemailText.setText(currentX509.issuerAltNameEmail);
				
				int choice = JOptionPane.showConfirmDialog(Sertificate.this, alternativeData,"Alternativne informacije", JOptionPane.OK_CANCEL_OPTION);
				if(choice == 0)
				{
					String ip = altIpText.getText();
					String dns = altdnsText.getText();
					String email = altemailText.getText();
					
					currentX509.issuerAltNameIp = ip.equals("")?null:ip;
					currentX509.issuerAltNameDns = dns.equals("")?null:dns;
					currentX509.issuerAltNameEmail = email.equals("")?null:email;
				}
				
			}
		});
		
		
		initUserData();
		korisnickiPodaci.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				cnText.setText(currentX509.subjectInfo.get("CN"));
				ouText.setText(currentX509.subjectInfo.get("OU"));
				countryText.setText(currentX509.subjectInfo.get("C"));
				stateText.setText(currentX509.subjectInfo.get("ST"));
				locText.setText(currentX509.subjectInfo.get("L"));
				orgText.setText(currentX509.subjectInfo.get("O"));
				streetText.setText(currentX509.subjectInfo.get("STREET"));
				
				int choice = JOptionPane.showConfirmDialog(Sertificate.this, userData,"Korisnicke informacije", JOptionPane.OK_CANCEL_OPTION);
			
				if(choice == 0)
				{
					//C", "O", "L", "OU", "CN", "ST", "STREET"
					currentX509.subjectInfo.put("CN", cnText.getText());
					currentX509.subjectInfo.put("OU", ouText.getText());
					currentX509.subjectInfo.put("C", countryText.getText());
					currentX509.subjectInfo.put("ST", stateText.getText());
					currentX509.subjectInfo.put("L", locText.getText());
					currentX509.subjectInfo.put("O", orgText.getText());
					currentX509.subjectInfo.put("STREET", streetText.getText()); 
				}
			}
		});

		
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 160, 527, 190);
		tabPanel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblEkstenzijeopciono = new JLabel("Ekstenzije (opciono)");
		lblEkstenzijeopciono.setBounds(20, 11, 177, 14);
		panel_1.add(lblEkstenzijeopciono);
		
	basic = new JCheckBox("osnovna ogranicenja");
		basic.setBounds(16, 31, 190, 23);
		panel_1.add(basic);
		
		alternative = new JCheckBox("alternativna imena izdavaoca");
		alternative.setBounds(16, 56, 195, 23);
		panel_1.add(alternative);
		
		alternative.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (alternative.isSelected())
					alternativnoPodaci.setEnabled(true);
				else
					alternativnoPodaci.setEnabled(false);
			}
		});
		
		key = new JCheckBox("koriscenje kljuca");
		key.setBounds(16, 82, 150, 23);
		panel_1.add(key);
		
	
		JLabel lblKriticnaEkstenzija = new JLabel("Kriticna ekstenzija");
		lblKriticnaEkstenzija.setBounds(200, 10, 110, 15);
		panel_1.add(lblKriticnaEkstenzija);
		
		JLabel aliasiLbl = new JLabel("Alias:");
		aliasiLbl.setBounds(new Rectangle(320, 10, 70, 15));
		panel_1.add(aliasiLbl);
		
		
		aliasiText = new JTextField();
		aliasiText.setBounds(320, 30, 200, 20);
		panel_1.add(aliasiText);
		
		
		basicYes = new JRadioButton("da");
		basicYes.setBounds(208, 31, 46, 23);
		panel_1.add(basicYes);
		
		basicNe = new JRadioButton("ne");
		basicNe.setBounds(254, 31, 46, 23);
		panel_1.add(basicNe);
		
		alternativeYes = new JRadioButton("da");
		alternativeYes.setBounds(208, 56, 46, 23);
		panel_1.add(alternativeYes);
		
		alternativeNo = new JRadioButton("ne");
		alternativeNo.setBounds(254, 56, 46, 23);
		panel_1.add(alternativeNo);
		
		keyYes = new JRadioButton("da");
		keyYes.setBounds(208, 82, 46, 23);
		panel_1.add(keyYes);
		
		keyNo = new JRadioButton("ne");
		keyNo.setBounds(254, 82, 46, 23);
		panel_1.add(keyNo);
		
		
		JButton osnovnobtn = new JButton("Osnovna ogranicenja");
		osnovnobtn.setBounds(350, 60, 170, 23);
		panel_1.add(osnovnobtn);
		
		dodatnoPanel = new JPanel();
		dodatnoPanel.setLayout(new GridLayout(1, 1));
		
		checkCA = new JCheckBox("CA");
		dodatnoPanel.add(checkCA);
		
		pathField = new JTextField();
		dodatnoPanel.add(pathField);
		
		
		osnovnobtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				if (currentX509.isCA)
					checkCA.setSelected(true);
				else
					checkCA.setSelected(false);
				pathField.setText("" + currentX509.pathLength);
				Object[] dugmici = {"Sacuvaj"};
				int rez = JOptionPane.showOptionDialog(Sertificate.this,dodatnoPanel , "Osnovna ogranicenja", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, dugmici, dugmici[0]);
				
				if(rez == 0)
				{
					currentX509.isCA = checkCA.isSelected();
					currentX509.pathLength = Integer.parseInt(pathField.getText());
				}
			}
		});
		
		JButton ekstenzijebtn = new JButton("Key usage dodatno");
		ekstenzijebtn.setBounds(350, 90, 150, 23);
		panel_1.add(ekstenzijebtn);
		
		initExtenzije();
		ekstenzijebtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				Object[] dugmici= {"Zapamti"};
				
				if (currentX509.digitalSignature)
					Sertificate.this.signature.setSelected(true);
				else
					Sertificate.this.signature.setSelected(false);
				if (currentX509.nonRepudiation)
					Sertificate.this.nonrepud.setSelected(true);
				else
					Sertificate.this.nonrepud.setSelected(false);
				if (currentX509.keyEncipherment)
					Sertificate.this.keyenc.setSelected(true);
				else
					Sertificate.this.keyenc.setSelected(false);
				if (currentX509.dataEncipherment)
					Sertificate.this.dataenc.setSelected(true);
				else
					Sertificate.this.dataenc.setSelected(false);
				if (currentX509.keyAgreement)
					Sertificate.this.agree.setSelected(true);
				else
					Sertificate.this.agree.setSelected(false);
				if (currentX509.keyCertSign)
					Sertificate.this.keycert.setSelected(true);
				else
					Sertificate.this.keycert.setSelected(false);
				if (currentX509.cRLSign)
					Sertificate.this.crl.setSelected(true);
				else
					Sertificate.this.crl.setSelected(false);
				if (currentX509.encipherOnly)
					Sertificate.this.encOnly.setSelected(true);
				else
					Sertificate.this.encOnly.setSelected(false);
				if (currentX509.decipherOnly)
					Sertificate.this.decOnly.setSelected(true);
				else
					Sertificate.this.decOnly.setSelected(false);
				int rez = JOptionPane.showOptionDialog(Sertificate.this, Sertificate.this.ekstenz, "Ekstenzije", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, dugmici, dugmici[0]);
				
				if(rez == 0)
				{
					currentX509.digitalSignature = Sertificate.this.signature.isSelected();
					currentX509.nonRepudiation = Sertificate.this.nonrepud.isSelected();
					currentX509.keyEncipherment = Sertificate.this.keyenc.isSelected();
					currentX509.dataEncipherment = Sertificate.this.dataenc.isSelected();
					currentX509.keyAgreement = Sertificate.this.agree.isSelected();
					currentX509.keyCertSign = Sertificate.this.keycert.isSelected();
					currentX509.cRLSign = Sertificate.this.crl.isSelected();
					currentX509.encipherOnly = Sertificate.this.encOnly.isSelected();
					currentX509.decipherOnly = Sertificate.this.decOnly.isSelected();
					
				}
				
				
			}
		});
		
		JButton btnSacuvajPar = new JButton("Sacuvaj par");
		btnSacuvajPar.setBounds(350, 120, 110, 23);
		panel_1.add(btnSacuvajPar);
		btnSacuvajPar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (aliasiText.getText().equals(""))
					return;
				Integer day= (Integer)spinner.getValue();
				Integer month = (Integer)spinner_1.getValue();
				Integer year = (Integer)spinner_2.getValue();
				Date dateIn = new Date(year - 1900, month - 1, day);
				day= (Integer)spinner_3.getValue();
				month = (Integer)spinner_4.getValue();
				year = (Integer)spinner_5.getValue();
				Date dateOut = new Date(year - 1900, month - 1, day);
				currentX509.notBefore = dateIn;
				currentX509.notAfter = dateOut;
				currentX509.serialNumber = textField_1.getText(); 
				currentX509.alias = aliasiText.getText();
				currentX509.keySize = Integer.parseInt(textField.getText());
				currentX509.makeKey(Integer.parseInt(textField.getText()));
				if (key.isSelected())
					currentX509.keyUsageActive = true;
				else
					currentX509.keyUsageActive = false;
				if (basic.isSelected())
					currentX509.hasBasicConstraint = true;
				else
					currentX509.hasBasicConstraint = false;
				if (basicYes.isSelected())
					currentX509.criticalBasicConstraint = true;
				else
					currentX509.criticalBasicConstraint = false;
				if (alternativeYes.isSelected())
					currentX509.issuerCritical = true;
				else
					currentX509.issuerCritical = false;
				if (keyYes.isSelected())
					currentX509.keyUsageCritical = true;
				else
					currentX509.keyUsageCritical = false;
				
				if (Controller.getInstance().addCert(currentX509))
					Sertificate.this.osveziCombo(currentX509);
				
			}

		
		});
		
	
		import_exportTab = new JPanel();
		tabbedPane.addTab("Import/eksport para", null, import_exportTab, null);
		import_exportTab.setLayout(null);
		
		eksport_sertifikata = new JPanel();
		tabbedPane.addTab("Eksport/potpis sertifikata", null, eksport_sertifikata, null);
		eksport_sertifikata.setLayout(null);
		
		ButtonGroup group = new ButtonGroup();

		group.add(keyYes);
		group.add(keyNo);
		

		ButtonGroup group2 = new ButtonGroup();
		group2.add(basicYes);
		group2.add(basicNe);
		
		ButtonGroup group1 = new ButtonGroup();
		group1.add(alternativeYes);
		group1.add(alternativeNo);
		
		initImportExport();
		initExportSertificate();
		
		restoreFields();
	}
	
	private void initExtenzije()
	{
		ekstenz = new JPanel();
		ekstenz.setLayout(new GridLayout(9, 1));
		
		signature = new JCheckBox("Digitalni potpis");
		ekstenz.add(signature);
		
		dataenc = new JCheckBox("Sifrovanje podatka");
		ekstenz.add(dataenc);
		
		keyenc = new JCheckBox("Sifrovanje kljuca");
		ekstenz.add(keyenc);
		
		keycert =   new JCheckBox("Potpisivanje");
		ekstenz.add(keycert);
		
		nonrepud =  new JCheckBox("Neodbijajuci");
		ekstenz.add(nonrepud);
		
		crl =  new JCheckBox("CRL potpis");
		ekstenz.add(crl);
		
		agree =  new JCheckBox("Ugovor"); //key agreement
		ekstenz.add(agree);
		
		encOnly =  new JCheckBox("Samo enkripcija");
		ekstenz.add(encOnly);
		
		decOnly = new JCheckBox("Samo dekiripcija");
		ekstenz.add(decOnly);
		
		
		
		
	}

	public LinkedList<String> parseAliases(String text) {
		if(text == null || text.equals(""))
		return null;
		
		LinkedList<String> ret = new LinkedList<String>();
		String tokens[] = text.split(",");
		
		for (String s: tokens)
		{
			ret.add(s);
		}
		
		return ret;
	}
	
	private void initAlternativeData()
	{
		alternativeData = new JPanel();
		alternativeData.setLayout(new GridLayout(3, 2));
		
		JLabel altIpLbl = new JLabel("Alternativna Ip adresa");
		alternativeData.add(altIpLbl);
		
		altIpText = new JTextField();
		alternativeData.add(altIpText);
		
		JLabel altdnsLbl = new JLabel("Alternativni dns");
		alternativeData.add(altdnsLbl);
		
		altdnsText = new JTextField();
		alternativeData.add(altdnsText);
		
		JLabel altemailLbl = new JLabel("Alternativna e-mail adresa");
		alternativeData.add(altemailLbl);
		
		altemailText = new JTextField();
		alternativeData.add(altemailText);
	}

	private void initUserData()
	{

		userData = new JPanel();
		userData.setLayout(new GridLayout(7, 2));
	/*	userData.setBounds(0, 0,300, 280);
		userData.setLayout(null);*/
		
		JLabel commonName = new JLabel("Common name (CN)");
		//commonName.setBounds(10, 20, 100, 20);
		userData.add(commonName);
		
		 cnText = new JTextField();
		//cnText.setBounds(120, 20, 100, 20);
		userData.add(cnText);
		
		JLabel ou = new JLabel("Organizational unit (OU)");
		//ou.setBounds(10, 50, 100, 20);
		userData.add(ou);
		
		ouText = new JTextField();
		//ouText.setBounds(120, 50, 100, 20);
		userData.add(ouText);
		
		JLabel org = new JLabel("Organization (O)");
		//org.setBounds(10, 80, 100, 20);
		userData.add(org);
		
		orgText = new JTextField();
		//orgText.setBounds(120,80, 100, 20);
		userData.add(orgText);
		
		JLabel loc = new JLabel("Locality (L)");
		//loc.setBounds(10, 110, 100, 20);
		userData.add(loc);
		
		locText = new JTextField();
		//locText.setBounds(120, 110, 100, 20);
		userData.add(locText);
		
		JLabel state = new JLabel("State name (S)");
		//state.setBounds(10, 140, 100, 20);
		userData.add(state);
		
		stateText = new JTextField();
		//stateText.setBounds(120, 140, 100, 20);
		userData.add(stateText);
		
		JLabel country = new JLabel("Country name (C)");
		//country.setBounds(10, 170, 100, 20);
		userData.add(country);
		
		countryText = new JTextField();
		countryText.setBounds(120, 170, 100, 20);
		userData.add(countryText);
		
		JLabel street = new JLabel("Street (STREET)");
		userData.add(street);
		
		streetText = new JTextField();
		streetText.setBounds(120, 180, 100, 20);
		userData.add(streetText);
	/*	JButton ok = new JButton("Potvrdite");
		//ok.setBounds(new Rectangle(20, 210, 90, 20));
		userData.add(ok);
		
		JButton nok = new JButton("Odustanite");
		//nok.setBounds(new Rectangle(150, 210, 90, 20));
		userData.add(nok);
		*/
	/*	ok.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		nok.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
		});*/
	}

	private void initExportSertificate() 
	{
		JLabel l = new JLabel();
		l.setText("Eksportuj CSR:");
		l.setLocation(29, 49);
		l.setSize(150, 20);
		eksport_sertifikata.add(l);
		
		comboBox = new JComboBox();
		comboBox.setBounds(29, 80, 150, 20);
		eksport_sertifikata.add(comboBox);
		
		for (Object o : Controller.getInstance().allCerts)
			comboBox.addItem(o);
		
		JButton btnEksportuj_1 = new JButton("Eksportuj");
		btnEksportuj_1.setBounds(29, 132, 100, 25);
		eksport_sertifikata.add(btnEksportuj_1);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(Color.DARK_GRAY);
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(263, 0, 35, 320);
		eksport_sertifikata.add(separator);
		
		JLabel lblPotpisiSertifikat = new JLabel("Potpisi sertifikat: ");
		lblPotpisiSertifikat.setBounds(309, 51, 150, 20);
		eksport_sertifikata.add(lblPotpisiSertifikat);
		
		comboBox_1 = new JComboBox();
		comboBox_1.setBounds(309, 79, 150, 20);
		eksport_sertifikata.add(comboBox_1);
		
		for (Object o : Controller.getInstance().allCerts)
			comboBox_1.addItem(o);
		
		JButton btnPotpisi = new JButton("Potpisi");
		btnPotpisi.setBounds(307, 132, 100, 25);
		eksport_sertifikata.add(btnPotpisi);
		
		btnEksportuj_1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (comboBox.getSelectedIndex() == -1)
					return;
				FilePicker picker = new FilePicker(Sertificate.this);
				String path = picker.selectPath();
				X509CertData data = (X509CertData)comboBox.getSelectedItem();
				data.doPkcs10(path);
			}
		});
		
		btnPotpisi.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (comboBox_1.getSelectedIndex() == -1)
					return;
				FilePicker picker = new FilePicker(Sertificate.this);
				String path = picker.selectPath();
				X509CertData data = (X509CertData)comboBox_1.getSelectedItem();
				data.doCert(path);
				
			}
		});
		
		
	
	}

	private void initImportExport()
	{
		
		JLabel lblImportujParKljuceva = new JLabel("Importuj par kljuceva:");
		lblImportujParKljuceva.setBounds(100, 40, 150, 20);
		import_exportTab.add(lblImportujParKljuceva);
		
		JButton btnImportuj = new JButton("Importuj");
		btnImportuj.setBounds(260, 40, 90, 25);
		import_exportTab.add(btnImportuj);
		
		
		JLabel aesLbl = new JLabel("AES enkripcija");
		aesLbl.setBounds(450, 40, 120, 15);
		//panel_3.add(aesLbl);
		
		aesBox = new JCheckBox("Enkripcija");
		aesBox.setBounds(100, 80, 120, 15);
		import_exportTab.add(aesBox);
		aesImportPass = new TextField();
		aesImportPass.setBounds(220, 80, 120, 30);
		import_exportTab.add(aesImportPass);
		
		JLabel lblEksportujParKljuceva = new JLabel("Eksportuj par kljuceva:");
		lblEksportujParKljuceva.setBounds(100, 171, 150, 20);
		import_exportTab.add(lblEksportujParKljuceva);
		
		JButton btnEksportuj = new JButton("Eksportuj");
		btnEksportuj.setBounds(260, 171, 90, 25);
		import_exportTab.add(btnEksportuj);
		
		exportCombo = new JComboBox();
		exportCombo.setBounds(100, 207, 143, 20);
		import_exportTab.add(exportCombo);
		
		for (Object o : Controller.getInstance().allCerts)
			exportCombo.addItem(o);
		
		aesOutBox = new JCheckBox("Enkripcija");
		aesOutBox.setBounds(260, 207, 100, 15);
		import_exportTab.add(aesOutBox);
		
		aesExportPass = new TextField();
		aesExportPass.setBounds(260, 240, 120, 30);
		import_exportTab.add(aesExportPass);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(Color.DARK_GRAY);
		separator.setBounds(0, 130, 550, 11);
		import_exportTab.add(separator);
		
		
		
	/*	JLabel lblPregledajKljuceve = new JLabel("Pregledaj par kljuceva:");
		lblPregledajKljuceve.setBounds(295, 21, 150, 20);
		import_exportTab.add(lblPregledajKljuceve);
		
		JButton btnPregledaj = new JButton("Pregledaj");
		btnPregledaj.setBounds(295, 97, 89, 23);
		import_exportTab.add(btnPregledaj);
		
		comboBox_pregled = new JComboBox();
		comboBox_pregled.setBounds(295, 60, 150, 20);
		import_exportTab.add(comboBox_pregled);
		
		textAreaPregled = new JTextArea();
		textAreaPregled.setBounds(295, 147, 182, 124);
		import_exportTab.add(textAreaPregled);*/
		
		btnImportuj.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				FilePicker picker = new FilePicker(Sertificate.this);
				String path = picker.selectPath();
				X509CertData newCert = new X509CertData();
				String password = Sertificate.this.aesImportPass.getText();
				boolean encrypt = Sertificate.this.aesBox.isSelected();
				if (!newCert.doInPkcs12(password, path, encrypt))
					return;
				if (Controller.getInstance().addCert(newCert))
				{
					currentX509 = newCert;
					Sertificate.this.osveziCombo(currentX509);
					restoreFields();
				}
			}
		});
		
		btnEksportuj.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (exportCombo.getSelectedIndex() == -1)
					return;
				FilePicker picker = new FilePicker(Sertificate.this);
				String path = picker.selectPath();
				String password = Sertificate.this.aesExportPass.getText();
				boolean encrypt = Sertificate.this.aesOutBox.isSelected();
				X509CertData data = (X509CertData)exportCombo.getSelectedItem();
				data.doOutPkcs12(password, path, encrypt);
				
			}
		});
		
	/*	btnPregledaj.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (comboBox_pregled.getSelectedIndex() == -1)
					return;
				X509CertData data = (X509CertData)comboBox_pregled.getSelectedItem();
				textAreaPregled.setText(data.getKeyDescription());
				
			}
		});*/
	}
	
	
	public void restoreFields()	
	{
		textField.setText("" + currentX509.keySize);
		Date dateIn =  currentX509.notBefore;
		spinner.setValue(dateIn.getDate());
		spinner_1.setValue(dateIn.getMonth() + 1);
		spinner_2.setValue(dateIn.getYear() + 1900);
		Date dateOut = currentX509.notAfter;
		spinner_3.setValue(dateOut.getDate());
		spinner_4.setValue(dateOut.getMonth() + 1);
		spinner_5.setValue(dateOut.getYear() + 1900);
		textField_1.setText(currentX509.serialNumber);
		aliasiText.setText(currentX509.alias);
		if (currentX509.keyUsageActive)
			key.setSelected(true);
		else
			key.setSelected(false);
		if (currentX509.hasBasicConstraint)
			basic.setSelected(true);
		else
			basic.setSelected(false);
		if (currentX509.criticalBasicConstraint)
		{
			basicYes.setSelected(true);
		}
		else
		{
			basicNe.setSelected(true);
		}
		
		if (currentX509.issuerAltNameDns != null || currentX509.issuerAltNameIp != null || currentX509.issuerAltNameEmail != null)
			alternative.setSelected(true);
		else
			alternative.setSelected(false);
		if (currentX509.issuerCritical)
			alternativeYes.setSelected(true);
		else
			alternativeNo.setSelected(true);
		if (currentX509.keyUsageCritical)
			keyYes.setSelected(true);
		else
			keyNo.setSelected(true);
	}
	public void osveziCombo(X509CertData item)
	{
		this.comboBox.addItem(item);
		this.comboBox_1.addItem(item);
		this.exportCombo.addItem(item);
		JMenuItem menuitems2[] = new JMenuItem[menuitems.length + 1];
		for (int i = 0; i < menuitems.length; ++i)
			menuitems2[i] = menuitems[i];
		String text = item.alias;
		menuitems2[menuitems.length] = new JMenuItem(text);
		menuitems = menuitems2;
		mnPregledajSertifikat.add(menuitems[menuitems.length - 1]);
		menuitems[menuitems.length - 1].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JMenuItem i = (JMenuItem)e.getSource();
				currentX509 = Controller.getInstance().findCert(i.getLabel());
				restoreFields();
				
			}
		});
	}
	
}
