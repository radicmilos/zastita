package gui;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;

import javax.swing.JFileChooser;

public class FilePicker extends JFileChooser{
	
	private Component parent;
	
	public FilePicker(Component parent)
	{
		this.parent = parent;
	}
	
	public String selectPath()
	{
		this.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int status = this.showSaveDialog(parent);
		if (status != JFileChooser.APPROVE_OPTION)
			return null;
		File f = this.getSelectedFile();
		return f.getAbsolutePath();
	}
	
	public boolean saveData(Object o)
	{
		this.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int status = this.showSaveDialog(parent);
		if (status != JFileChooser.APPROVE_OPTION)
			return false;
		File f = this.getSelectedFile();
		try {
			if (!f.exists())
				f.createNewFile();
			FileOutputStream stream = new FileOutputStream(f);
			PrintWriter output = new PrintWriter(stream);
			output.write(o.toString());
			output.close();
			stream.close();
			return true;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		
	}
	
	public String loadData()
	{
		this.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int status = this.showOpenDialog(parent);
		if (status != JFileChooser.APPROVE_OPTION)
			return null;
		File f = this.getSelectedFile();
		try {
			FileInputStream stream = new FileInputStream(f);
			FileReader input = new FileReader(f);
			String toRet = "";
			BufferedReader reader = new BufferedReader(input);
			String temp;
			while ((temp = reader.readLine()) != null)
			{
				toRet += temp;
			}
			input.close();
			stream.close();
			return toRet;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

}
