package cryptography;

import java.security.PrivateKey;
import java.security.Signature;

public class Sign {

	public static byte[] sign(PrivateKey key, byte[] data)
	{
		try {
			Signature signature = Signature.getInstance("SHA1withRSA");
			signature.initSign(key);
			signature.update(data);
			return signature.sign();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
}
