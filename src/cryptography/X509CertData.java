package cryptography;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.Attribute;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.X509V3CertificateGenerator;

public class X509CertData implements Serializable{

	
    public int version = 3;
    public int keySize = 1024;
    public String serialNumber;
    public Date notAfter = new Date();
    public Date notBefore = new Date();
    
    public boolean digitalSignature;
    public boolean nonRepudiation;
    public boolean keyEncipherment;
    public boolean dataEncipherment;
    public boolean keyAgreement;
    public boolean keyCertSign;
    public boolean cRLSign;
    public boolean encipherOnly;
    public boolean decipherOnly;
    
    public Map<String, String> subjectInfo = new HashMap<String, String> ();

    public boolean issuerCritical;
    public String issuerAltNameDns;
    public String issuerAltNameIp;
    public String issuerAltNameEmail;
    public String issuerAltNameOther;

    public boolean hasBasicConstraint;
    public boolean criticalBasicConstraint;
    public boolean isCA;
    public int pathLength;
    public boolean hasLengthLimit;

    public boolean keyUsageActive;
    public boolean keyUsageCritical;
    
    
    public KeyPair key;
    
    public String alias;
    
    public transient PKCS10CertificationRequest pkcs10;
    
    public transient X509Certificate cert;
    
    public static final String subjectShortcuts[] = {"C", "O", "L", "OU", "CN", "ST", "STREET"};
    
    static 
    {
    	Security.addProvider(new BouncyCastleProvider());
    }
    
    public void makeKey(int size)
    {
		try {
			KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
			gen.initialize(size);
			key = gen.generateKeyPair();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public GeneralNames fillAltName()
    {
        
            List<GeneralName> genNames = new LinkedList<GeneralName> ();
            if (this.issuerAltNameDns != null)
            	genNames.add(new GeneralName(GeneralName.dNSName, this.issuerAltNameDns));
            if (this.issuerAltNameIp != null)
            	genNames.add(new GeneralName(GeneralName.iPAddress, this.issuerAltNameIp));
            if (this.issuerAltNameEmail != null)
            	genNames.add(new GeneralName(GeneralName.rfc822Name, this.issuerAltNameEmail));
            if (this.issuerAltNameOther != null)
            	genNames.add(new GeneralName(GeneralName.otherName, this.issuerAltNameOther));

            GeneralNames alt = null;
            GeneralName[] gnArr = new GeneralName[genNames.size()];
            for (int i = 0; i < genNames.size(); ++i)
            	gnArr[i] = genNames.get(i);
            if (genNames.size() > 0)
            	alt = new GeneralNames(gnArr);
            return alt;

    }
    
    
    public boolean restoreCritical(X509Certificate certificate)
    {
    	Collection<String> criticalExtensions = certificate.getCriticalExtensionOIDs();
    	if (criticalExtensions != null)
    	{
    		if (criticalExtensions.contains("2.5.29.19"))
    			this.criticalBasicConstraint = true;
    		else
    			this.criticalBasicConstraint = false;
    		if (criticalExtensions.contains("2.5.29.15"))
    			this.keyUsageCritical = true;
    		else
    			this.keyUsageCritical = false;
    		if (criticalExtensions.contains("2.5.29.18"))
    			this.issuerCritical = true;
    		else
    			this.issuerCritical = false;
    	}
    	else
    	{
    		this.criticalBasicConstraint = false;
    		this.keyUsageCritical = false;
    		this.issuerCritical = false;
    	}
    	return true;
    }
    public boolean restoreAltName(X509Certificate certificate)
    {
    	Collection<List<?>> certAlt = null;
		try {
			certAlt = certificate.getIssuerAlternativeNames();
		} catch (CertificateParsingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
        if (certAlt != null) {

        	List<?> altName;
        	Iterator <List <?> >iter = certAlt.iterator();
            while (iter.hasNext())
            {
            	altName = iter.next();
            for (int i = 0; i < altName.size(); i += 2)
            {
                int type = (Integer) altName.get(i);
                String currentAlt = (String)altName.get(i + 1);
                if (type == GeneralName.dNSName)
                	issuerAltNameDns = currentAlt;
                else if (type == GeneralName.iPAddress)
                	issuerAltNameIp = currentAlt;
                else if (type == GeneralName.rfc822Name)
                	issuerAltNameEmail = currentAlt;
                else
                	issuerAltNameOther = currentAlt;
            }
            }
        }
        else
        {
        	this.issuerAltNameDns = null;
        	this.issuerAltNameIp = null;
        	this.issuerAltNameEmail = null;
        	this.issuerAltNameOther = null;
        }
        return true;
    }
    
    
    public int getKeyUsageVal()
    {
    	int keyVal = 0;
    	keyVal += digitalSignature?KeyUsage.digitalSignature:0;
    	keyVal += nonRepudiation?KeyUsage.nonRepudiation:0;
    	keyVal += keyEncipherment?KeyUsage.keyEncipherment:0;
    	keyVal += dataEncipherment?KeyUsage.dataEncipherment:0;
    	keyVal += keyAgreement?KeyUsage.keyAgreement:0;
    	keyVal += keyCertSign?KeyUsage.keyCertSign:0;
    	keyVal += cRLSign?KeyUsage.cRLSign:0;
    	keyVal += encipherOnly?KeyUsage.encipherOnly:0;
    	keyVal += decipherOnly?KeyUsage.decipherOnly:0;
    	return keyVal;
    }
    
    public boolean makeCert()
    {
    	try{
    	X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
    	
    	certGen.setNotAfter(notAfter);
    	certGen.setNotBefore(notBefore);
    	
    	X500Principal sub = getSubject();
    	certGen.setSubjectDN(sub);
    	certGen.setIssuerDN(sub); //self signed
    	certGen.setSerialNumber(new BigInteger(serialNumber));
    	
    	certGen.setPublicKey(key.getPublic());
    	certGen.setSignatureAlgorithm("SHA1withRSA");
    	
    	BasicConstraints constraint;
    	if (!isCA)
    		constraint = new BasicConstraints(false);
    	else
    		constraint = new BasicConstraints(this.pathLength);
    	if (this.hasBasicConstraint)
    		certGen.addExtension(X509Extensions.BasicConstraints, this.criticalBasicConstraint, constraint);
    	
    	
    	int keyVal = getKeyUsageVal();
    	
    	if (this.keyUsageActive)
    		certGen.addExtension(X509Extensions.KeyUsage,this.keyUsageCritical, new KeyUsage(keyVal));
    	
    	GeneralNames gn = fillAltName();
    	if (gn != null)
    		certGen.addExtension(X509Extensions.IssuerAlternativeName, issuerCritical, gn);
    	
    	PrivateKey keyPrivate = key.getPrivate();
    	cert = certGen.generate(keyPrivate); //self signed

    	}
    	catch(Exception e) {e.printStackTrace();
    	return false;}
    	return true;
    }
    
	public boolean outputCert(String path) {
        try (
        	FileOutputStream fileStream = new FileOutputStream(new File(path));
        	PrintWriter out = new PrintWriter(fileStream);
        	)
        {
            String content = Base64.getEncoder().encodeToString(cert.getEncoded());
            out.println("-----BEGIN CERTIFICATE-----");
            out.write(content);
            out.println("-----END CERTIFICATE-----");
            out.flush();
            fileStream.flush();
        }
        catch (Exception e) {return false;}
        return true;
    }
	
	public X500Principal getSubject() {
		X500Principal subject = new X500Principal("CN = " + subjectInfo.get("CN") + ", ST = " + subjectInfo.get("ST") + ", STREET = " +
				subjectInfo.get("STREET") + ", OU = " + subjectInfo.get("OU") + ", C = " + subjectInfo.get("C") + ", O = " + subjectInfo.get("O") + ", L = " + subjectInfo.get("L"));
		
		return subject;
	}
	
	public ASN1Set getAttributes() throws Exception
	{

            Vector<X509Extension> values = new Vector<X509Extension>();
            Vector<ASN1ObjectIdentifier> oids = new Vector<ASN1ObjectIdentifier>();
           
            GeneralNames gn = fillAltName();
            if (gn != null)
            {
            	oids.add(X509Extensions.IssuerAlternativeName);
                values.add(new X509Extension(issuerCritical, new DEROctetString(gn)));
            }
            
            if (keyUsageActive) {
                int keyVal = getKeyUsageVal();

                X509Extension keyUsage = new X509Extension(keyUsageCritical, new DEROctetString(new KeyUsage(keyVal)));

                oids.add(X509Extensions.KeyUsage);
                values.add(keyUsage);
            }


            if (this.hasBasicConstraint) {
            	BasicConstraints bc;
            	if (this.isCA)
            		bc = new BasicConstraints(this.pathLength);
            	else
            		bc = new BasicConstraints(false);
                	
                values.add(new X509Extension(this.criticalBasicConstraint, new DEROctetString(bc)));
                oids.add(X509Extensions.BasicConstraints);
            }

            X509Extensions extensions = new X509Extensions(oids, values);
            Attribute attribute = new Attribute(
                    PKCSObjectIdentifiers.pkcs_9_at_extensionRequest,
                    new DERSet(extensions));
            ASN1Set attributes = new DERSet(attribute);

            return attributes;

	}
	
	public boolean makePkcs10()
	{
		try {
			
			pkcs10 = new PKCS10CertificationRequest("SHA1withRSA", getSubject(), key.getPublic(), 
			        getAttributes(), key.getPrivate());
		} 
		catch(Exception e) {e.printStackTrace();return false;}
		return true;
	}
	
	public boolean outputPkcs10(String path)
	{
        try (
            	FileOutputStream fileStream = new FileOutputStream(new File(path));
            	PrintWriter out = new PrintWriter(fileStream);
            	)
            {
                String content = Base64.getEncoder().encodeToString(pkcs10.getEncoded());
                out.println("-----BEGIN CERTIFICATE REQUEST-----");
                out.write(content);
                out.println("\n-----END CERTIFICATE REQUEST-----");
                out.flush();
                fileStream.flush();
            }
            catch (Exception e) {return false;}
            return true;		
	}
	
	public boolean doCert(String path)
	{
		if (makeCert())
			if (outputCert(path))
				return true;
		return false;
	}

	public boolean doPkcs10(String path)
	{
		if (makePkcs10())
			if (outputPkcs10(path))
				return true;
		return false;
	}
	
	public byte[] makePkcs12(String password)
	{
		KeyStore keyS = KeyStores.createKeyStore();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		if (!makeCert())
			return null;
		try {
			keyS.setCertificateEntry(alias, cert);
			Certificate[] chain = new Certificate[1];
			chain[0] = cert;
			keyS.setKeyEntry(alias, key.getPrivate(), password.toCharArray(), chain);
			keyS.store(stream, password.toCharArray());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return stream.toByteArray();
	}
	
	public boolean outputPkcs12(byte[] data, String password, String path, boolean enecrypt)
	{
        try (
            FileOutputStream fileStream = new FileOutputStream(new File(path));
            )
        {
        	if (enecrypt)
        		return AES.encrypt(data, password, fileStream);
        	else 
        		fileStream.write(data);
        		
     	}
        catch(Exception e) {e.printStackTrace(); return false;}
        return true;
	}
	
	public byte[] inputPkcs12(String password, String path, boolean encrypt)
	{
        try (
                FileInputStream fileStream = new FileInputStream(new File(path));
            )
        {
        	if (encrypt)
        		return AES.decrypt(password, fileStream);
        	else
        	{
    			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    			byte [] data = new byte[100];
    			int numRead;
    			while ((numRead = fileStream.read(data, 0, data.length)) != -1) {
    				  buffer.write(data, 0, numRead);
    				}
    			buffer.flush();
    			data = buffer.toByteArray();
    			return data;
        	}
        }
        catch(Exception e) {return null;}
	}
	
	public void getSubjectInfo(X509Certificate cert)
	{
		int position;
		int position2;
		String val;
		subjectInfo.clear();
		String subjectInfoString = cert.getSubjectDN().getName();
		System.out.println(subjectInfoString);
		for (String s : subjectShortcuts)
		{
			position = subjectInfoString.indexOf(s + "=");
			if (position != -1)
			{
				position2 = subjectInfoString.indexOf(",", position);
				if (position2 == -1)
					position2 = subjectInfoString.length();
				val = subjectInfoString.substring(position + s.length() + 1, position2);
			}
			else
				val = "";
			subjectInfo.put(s, val);
		}
	}
	
	public boolean restorePkcs12(byte[] data, String password)
	{
		try {

			ByteArrayInputStream stream = new ByteArrayInputStream(data);
			KeyStore keyS = KeyStores.createKeyStore(stream, password);
			alias = keyS.aliases().nextElement();
			cert = (X509Certificate) keyS.getCertificate(alias);
			notAfter = cert.getNotAfter();
			notBefore = cert.getNotBefore();
			serialNumber = cert.getSerialNumber().toString();
			PublicKey publicK = cert.getPublicKey();
			PrivateKey privateK = (PrivateKey)keyS.getKey(alias, password.toCharArray());
			key = new KeyPair(publicK, privateK);
			
            byte[] extension = cert.getExtensionValue("2.5.29.19") ;
            if (extension != null)
            {
                hasBasicConstraint = true;
                this.pathLength = cert.getBasicConstraints();
                this.hasLengthLimit = (pathLength == Integer.MAX_VALUE?true:false);
                if (this.pathLength != -1)
                	this.isCA = true;
                else
                	this.isCA = false;
            }
			
			getSubjectInfo(cert);
			restoreAltName(cert);
			restoreCritical(cert);
			
			if (cert.getKeyUsage() != null)
			{
				this.keyUsageActive = true;
				this.digitalSignature = cert.getKeyUsage()[0];
				this.nonRepudiation = cert.getKeyUsage()[1];
				this.keyEncipherment = cert.getKeyUsage()[2];
				this.dataEncipherment = cert.getKeyUsage()[3];
				this.keyAgreement = cert.getKeyUsage()[4];
				this.keyCertSign = cert.getKeyUsage()[5];
				this.cRLSign = cert.getKeyUsage()[6];
				this.encipherOnly = cert.getKeyUsage()[7];
				this.decipherOnly = cert.getKeyUsage()[8];
				
			}
			else
				this.keyUsageActive = false;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean doOutPkcs12(String password, String path, boolean encrypt)
	{
		byte[] data = makePkcs12(password);
		if (data == null)
			return false;
		return outputPkcs12(data, password, path, encrypt);
	}
	
	public boolean doInPkcs12(String password, String path, boolean encrypt)
	{
		byte[] data = inputPkcs12(password, path, encrypt);
		if (data == null)
			return false;
		return restorePkcs12(data, password);
	}
	
	public String toString()
	{
		return alias;
	}
	
	public String getKeyDescription()
	{
		PublicKey pu = key.getPublic();
		PrivateKey pr = key.getPrivate();
		
		return pu.toString() + "\n" + pr.toString();
	}
	
	public boolean equals(Object other)
	{
		if (other instanceof X509CertData)
		{
			return ((X509CertData)other).alias.equals(alias);
		}
		else
			return super.equals(other);
	}
}
