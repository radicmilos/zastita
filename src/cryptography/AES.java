package cryptography;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES {
	
	private static String aesType = "AES/CFB8/NoPadding";
	
	public static SecretKey genKey(byte[] data)
	{
		byte data2[] = new byte[16];
		for (int i = 0; i < 16; ++i)
			data2[i] = data[i];
		return new SecretKeySpec(data2, "AES");
		
	}
	
	public static IvParameterSpec genIV(byte[] data)
	{
		byte data2[] = new byte[16];
		for (int i = 16; i < 32; ++i)
			data2[i - 16] = data[i];
		IvParameterSpec IV = new IvParameterSpec(data2);
		return IV;
	}
	
    public static byte[] sha(String key) {
    	byte[] hash = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            hash = digest.digest(key.getBytes());
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return hash;
    }
    
    public static boolean encrypt(byte[] data, String key, OutputStream output)
    {
    	byte[] shaData = sha(key);
    	SecretKey aesKey = genKey(shaData);
    	IvParameterSpec iv = genIV(shaData);
    	return encrypt(data, aesKey, iv, output);
    }
    
    public static byte[] decrypt(String key, InputStream input)
    {
    	byte[] shaData = sha(key);
    	SecretKey aesKey = genKey(shaData);
    	IvParameterSpec iv = genIV(shaData);
    	return decrypt(aesKey, iv, input);
    }
    
	public static boolean encrypt(byte[] data, SecretKey key, IvParameterSpec IV, OutputStream output)
	{
		try {
			Cipher cipher = Cipher.getInstance(aesType);
			cipher.init(Cipher.ENCRYPT_MODE, key, IV);
			byte[] encrypted = cipher.doFinal(data);
			output.write(encrypted);
			output.close();
			return true;
		} catch (Exception e) { 
			e.printStackTrace();
			return false;
		}
		
	}
	
	public static byte[] decrypt(SecretKey key, IvParameterSpec IV, InputStream input)
	{
		byte[] decrypted = null;
		
		try {
			Cipher cipher = Cipher.getInstance(aesType);
			cipher.init(Cipher.DECRYPT_MODE, key, IV);
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			byte [] data = new byte[100];
			int numRead;
			while ((numRead = input.read(data, 0, data.length)) != -1) {
				  buffer.write(data, 0, numRead);
				}
			buffer.flush();
			data = buffer.toByteArray();
			decrypted = cipher.doFinal(data);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			}
		
		return decrypted;
	}
}
