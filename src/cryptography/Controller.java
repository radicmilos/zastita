package cryptography;

import java.util.LinkedList;
import java.util.List;

public class Controller {

	private static Controller instance = new Controller();
	
	public List<X509CertData> allCerts = new LinkedList<X509CertData> ();
	
	public static Controller getInstance()
	{
		return instance;
	}
	
	public boolean addCert(X509CertData cert)
	{
		if (!allCerts.contains(cert))
		{
			allCerts.add(cert);
			return true;
		}
		else
			return false;
	}
	
	public X509CertData findCert(String alias)
	{
		for (X509CertData cert : allCerts)
		{
			if (cert.alias.equals(alias))
				return cert;
		}
		return null;
	}
}
