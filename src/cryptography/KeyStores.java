package cryptography;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStore.Entry;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class KeyStores {
	
	
	public static KeyStore tekKeyStore;
	public static String path;
	public static char[] password;
	public static String keyStoreInstance = "PKCS12";	

	public static KeyStore getStoreByPath(char[] pass, String path)
	{
			
		password = pass;
		KeyStores.path = path;
		try {
			tekKeyStore = KeyStore.getInstance(keyStoreInstance);
			tekKeyStore.load(new FileInputStream(path), password);
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		return tekKeyStore;
	}
	
	public static void saveKeyStore(char[] pass, String path)
	{
		try {
			if(tekKeyStore == null)	
					tekKeyStore = KeyStore.getInstance(keyStoreInstance);
			
			tekKeyStore.load(null, pass);
			tekKeyStore.store(new FileOutputStream(path), pass);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	
	public static KeyStore createKeyStore()
	{
        KeyStore keyStore = null;
		try {
			keyStore = KeyStore.getInstance("PKCS12");
			keyStore.load(null, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return keyStore;  
	}
	
	public static KeyStore createKeyStore(InputStream inputStream, String password)
	{
		KeyStore keyStore = null;
		try {
			keyStore = KeyStore.getInstance("PKCS12");
			keyStore.load(inputStream, password.toCharArray());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return keyStore;
	}
	
	public static KeyStore createKeyStore(char []pass, String path)
	{
		
		KeyStores.path = path;
		KeyStores.password = pass;
		
		
		try {
			tekKeyStore = KeyStore.getInstance(keyStoreInstance);
			tekKeyStore.load(null, null);
			tekKeyStore.store(new FileOutputStream(path), pass);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return tekKeyStore;
	}
	
	public static void newKeyPair(char[] pass, String path, String keyPairName)
	{
		
		PasswordProtection pp = new KeyStore.PasswordProtection(pass);
		try {
			KeyStore k = KeyStore.getInstance(keyStoreInstance);
			k.load(null, pass);
			Entry e = k.getEntry(keyPairName, pp);
			k.setEntry(keyPairName, e, pp);
			k.store(new FileOutputStream(path), pass);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static ArrayList<String> getEntries()
	{
		ArrayList<String> entries = null;
		try {
		entries = Collections.list(tekKeyStore.aliases());
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return entries;
	}

}
