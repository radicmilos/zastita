import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import cryptography.AES;
import cryptography.X509CertData;


public class Test {
	public static void main(String [] args)
	{  
		X509CertData k = new X509CertData();
		k.makeKey(1024);
		for (String s : X509CertData.subjectShortcuts)
			k.subjectInfo.put(s, "skk");
		k.isCA = true;
		k.issuerCritical = true;
		k.hasBasicConstraint = true;
		k.decipherOnly = true;
		k.keyUsageActive = true;
		k.keyUsageCritical = true;
		k.doOutPkcs12("jak", "smor.pkcs12", true);		
	}

}
